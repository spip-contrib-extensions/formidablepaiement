<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'formidablepaiement_description' => 'Permettre de déclencher un paiement suite à la saisie d\'un formulaire généré par le plugin Formidable',
	'formidablepaiement_nom' => 'Formulaires de paiement',
	'formidablepaiement_slogan' => 'Extension du plugin formidable',
);

?>