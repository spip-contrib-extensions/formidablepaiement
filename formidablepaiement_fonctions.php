<?php
/**
 * Utilisations de pipelines par Formulaires de paiement
 *
 * @plugin     Formulaires de paiement
 * @copyright  2014
 * @author     Cédric Morin
 * @licence    GNU/GPL
 * @package    SPIP\Formidablepaiement\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')){
	return;
}

/**
 * Calculer le parrain pour une transaction liée à un formulaire formidable
 * @param int $id_formulaire
 * @return string
 */
function formidablepaiement_parrain($id_formulaire) {
	return "formidable:$id_formulaire";
}

/**
 * Verifier si le parrain d'une transaction est celui d'un formulaire formidable
 * @param string $parrain
 * @return false|mixed|null
 */
function formidablepaiement_is_parrain_formidable($parrain) {
	if (strpos($parrain, 'formidable:') === 0
	  and $parrain = explode(':', $parrain)
	  and count($parrain) === 2
	  and is_numeric(end($parrain))) {
		return $id_formulaire = array_pop($parrain);
	}

	return false;
}

/**
 * Afficher le lien vers une réponse formidable sur les transactions liées à des formulaires formidables
 * @param int $id_transaction
 * @param int $id_formulaires_reponse
 * @return string
 */
function bank_affiche_parrain_formidable($id_transaction, $id_formulaires_reponse) {
	$url = generer_url_entite($id_formulaires_reponse, 'formulaires_reponse');
	$t = objet_icone('formulaires_reponse', 12) . " #$id_formulaires_reponse";
	$title = _T(objet_info('formulaires_reponse', 'texte_objet')) . " #$id_formulaires_reponse";
	$title = attribut_html($title);
	$s = "<a href=\"$url\" title=\"$title\">$t</a>";
	$id_formulaire = sql_getfetsel('id_formulaire', 'spip_formulaires_reponses', 'id_formulaires_reponse='.intval($id_formulaires_reponse));
	if ($id_formulaire and $titre = generer_info_entite($id_formulaire, 'formulaire', 'titre')) {
		$title = attribut_html(textebrut($titre));
		$url = generer_url_entite($id_formulaire, 'formulaire');
		$t = objet_icone('formulaire', 12) . " #$id_formulaire";
		$s = "<a href=\"$url\" title=\"$title\">$t</a> $s";
	}
	return $s;
}
