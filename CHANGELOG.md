# Changelog
## 2.3.0 - 2024-01-24
### Changed

- Compatible aussi formidable v7
## 2.1.2

### Changed

- !3 : permettre un paiement unique pour les formulaires dont la réponse est éditable (une seule réponse par visiteur)

## 2.1.1 2024-02-26

- Compatibilité avec formidable 6.0.0

### Changed

- Indiquer explicitement que le traitement nécessite le traitement `enregistrement`
- Ajouter un nettoyeur avant la liste des transactions car il y a parfois un bouton float avant

## 2.0.4 2022-05-31

- Compatibilité avec formidable 5.2.0, qui utilise `json_encode()` pour sérialiser les traitements.
