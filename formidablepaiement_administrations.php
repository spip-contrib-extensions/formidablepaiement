<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Formulaires de paiement
 *
 * @plugin     Formulaires de paiement
 * @copyright  2014
 * @author     Cédric Morin
 * @licence    GNU/GPL
 * @package    SPIP\Formidablepaiement\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')){
	return;
}


/**
 * Fonction d'installation et de mise à jour du plugin Formulaires de paiement.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
 **/
function formidablepaiement_upgrade($nom_meta_base_version, $version_cible){
	$maj = [];

	$maj['1.0.1'] = [
		['formidablepaiement_upgrade_parrains']
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Transformer les anciens parrains formXX:YYYYY en formidable:XX
 * @return void
 */
function formidablepaiement_upgrade_parrains() {

	include_spip('formidablepaiement_fonctions');
	$res = sql_select('id_transaction, parrain, tracking_id', 'spip_transactions', "parrain LIKE 'form%:%'", '', 'id_transaction');
	while ($row = sql_fetch($res)) {
		if (preg_match(',^form(\d+):,', $row['parrain'], $m)) {
			$id_formulaire = $m[1];
			$parrain = formidablepaiement_parrain($id_formulaire);
			sql_updateq('spip_transactions', ['parrain' => $parrain], 'id_transaction='.intval($row['id_transaction']));
		}
		if (time() > _TIME_OUT) {
			return;
		}
	}
}


/**
 * Fonction de désinstallation du plugin Formulaires de paiement.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
 **/
function formidablepaiement_vider_tables($nom_meta_base_version){


	effacer_meta($nom_meta_base_version);
}
