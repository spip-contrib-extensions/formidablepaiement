<?php
/**
 * Traitement paiement apres la saisie d'un formulaire
 *
 * @plugin     Formulaires de paiement
 * @copyright  2014
 * @author     Cédric Morin
 * @licence    GNU/GPL
 * @package    SPIP\Formidablepaiement\traiter\paiement
 */

if (!defined('_ECRIRE_INC_VERSION')){
	return;
}

function traiter_paiement_dist($args, $retours){
	$formulaire = $args['formulaire'];
	$options = $args['options'];

	// il faut d'abord avoir enregistre
	if (
		!isset($retours['traitements']['enregistrement'])
	  or !$id_formulaires_reponse = $retours['id_formulaires_reponse']
	) {
		return $retours;
	}

	// Si paiement unique, on cherche s'il y a déjà un paiement ok ou attente pour cette réponse enregistrée
	if (
		!empty($options['paiement_unique'])
		and $transaction_ok = sql_fetsel(
			'id_transaction',
			'spip_transactions',
			array(
				'parrain LIKE ' . sql_quote('form'.$formulaire['id_formulaire'].':%'),
				'tracking_id = ' . intval($id_formulaires_reponse),
				sql_in('statut', array('ok', 'attente')),
			)
		)
	) {
		// Et on s'arrête
		return $retours;
	}

	$montant_ht = $montant_ttc = 0;
	// recuperer le montant

	// montant fixe/par defaut
	if ($options['montant_fixe']){
		$montant_ht = $montant_ttc = $options['montant_fixe'];
	}
	// saisie dans le formulaire

	if ($options['champ_montant']
		and $m = _request($options['champ_montant'])
		and is_numeric($m)){
		$montant_ht = $montant_ttc = $m;
	}

	// tva ?
	if (floatval($tva = $options['tva'])){
		if ($options['taxes']=="ttc"){
			$montant_ht = $montant_ttc/((100+$tva)/100);
		}
		if ($options['taxes']=="ht"){
			$montant_ttc = $montant_ht*((100+$tva)/100);
		}
	}

	if ($options['champ_auteur'] and $auteur = _request($options['champ_auteur'])){
		include_spip('inc/filtres');
		$auteur = email_valide($auteur);
	}

	// preparer la transaction
	include_spip('formidablepaiement_fonctions');
	$options = array(
		'montant_ht' => $montant_ht,
		'id_auteur' => (isset($GLOBALS['visiteur_session']['id_auteur']) ? $GLOBALS['visiteur_session']['id_auteur'] : 0),
		'auteur' => $auteur,
		'parrain' => formidablepaiement_parrain($formulaire['id_formulaire']),
		'tracking_id' => $id_formulaires_reponse,
	);

	$inserer_transaction = charger_fonction("inserer_transaction", "bank");
	$id_transaction = $inserer_transaction($montant_ttc, $options);

	// inserer le form de paiement dans le message_ok
	// il sera deplace dans le html par le pipeline formulaire_fond
	if ($id_transaction
		and $hash = sql_getfetsel("transaction_hash", "spip_transactions", "id_transaction=" . intval($id_transaction))){
		$form = recuperer_fond(
			"modeles/formidablepaiement-transaction",
			array(
				'id_transaction' => $id_transaction,
				'transaction_hash' => $hash,
				'id_formulaires_reponse' => $id_formulaires_reponse,
			)
		);

		include_spip('inc/securiser_action');
		$id = md5(@getmypid() . secret_du_site());
		$GLOBALS['formidable_post_' . $id] = $form;
	}

	// noter qu'on a deja fait le boulot, pour ne pas risquer double appel
	$retours['traitements']['paiement'] = true;
	return $retours;
}
