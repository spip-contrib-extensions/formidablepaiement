<?php
/**
 * Utilisations de pipelines par Formulaires de paiement
 *
 * @plugin     Formulaires de paiement
 * @copyright  2014
 * @author     Cédric Morin
 * @licence    GNU/GPL
 * @package    SPIP\Formidablepaiement\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')){
	return;
}

include_spip('formidablepaiement_fonctions');

/**
 * Rediriger vers le checkout apres un paiement qui demande confirmation
 * (type paypal express)
 * on saute l'etape de confirmation et on va direct au paiement
 *
 * @param $flux
 * @return mixed
 */
function formidablepaiement_formulaire_charger($flux){

	// gerer le retour paiement avec demande de confirmation
	if (_request('confirm')
		and $flux['args']['form']=='formidable'
		and $id = $flux['args']['args'][0]
		and isset($_SESSION['id_transaction'])
		and $id_transaction = $_SESSION['id_transaction']
		and $checkout = _request('checkout')
		and $trans = sql_fetsel("*", "spip_transactions", "id_transaction=" . intval($id_transaction))){

		// verifier que la transaction en session est bien associee a ce formulaire
		$id_formulaire = formidable_id_formulaire($id);
		if ($trans['parrain'] === formidablepaiement_parrain($id_formulaire)){
			// on reaffiche le modele de paiement qui demande confirmation
			$form = recuperer_fond("modeles/formidablepaiement-transaction", array('id_transaction' => $id_transaction, 'transaction_hash' => $trans['transaction_hash']));
			$flux['data'] =
				"<div class='formulaire_spip formulaire_paiement'>"
				. $form
				. "</div>";

			$css = find_in_path("css/formidablepaiement.css");
			$flux['data'] .= "<style type='text/css'>@import url('" . $css . "');</style>";

			// Alternative par define ?
			// on redirige directement vers le paiment (url de checkout)
			// car on a rien a reafficher
			#include_spip('inc/headers');
			#redirige_par_entete($checkout);
		}
	}
	return $flux;
}


/**
 * Mise en forme du formulaire de paiement post-saisie
 *
 * @param array $flux
 * @return array
 */
function formidablepaiement_formulaire_fond($flux){

	if ($flux['args']['form']=='formidable'
		and $id = $flux['args']['args'][0]
		and $flux['args']['je_suis_poste']){

		include_spip('inc/securiser_action');
		$id = md5(@getmypid() . secret_du_site());
		if (isset($GLOBALS['formidable_post_' . $id])
			and $form = $GLOBALS['formidable_post_' . $id]){
			$flux['data'] .=
				"<div class='formulaire_spip formulaire_paiement'>"
				. $form
				. "</div>";

			$css = find_in_path("css/formidablepaiement.css");
			$flux['data'] .= "<style type='text/css'>@import url('" . $css . "');</style>";
		}
	}
	return $flux;
}


/**
 * Renseigner les infos du payeur pour la demande de paiement (DSP2)
 * @param array $flux
 * @return array
 */
function formidablepaiement_bank_dsp2_renseigner_facturation($flux){
	// si c'est une transaction associee a un form
	if ($id_transaction = $flux['args']['id_transaction']
		and $id_formulaire = formidablepaiement_is_parrain_formidable($flux['args']['parrain'])
		and $id_formulaires_reponse = $flux['args']['tracking_id']){

		include_spip('formidable_fonctions');

		$reponse = sql_fetsel('*', 'spip_formulaires_reponses', 'id_formulaires_reponse=' . intval($id_formulaires_reponse));
		$formulaire = sql_fetsel('*', 'spip_formulaires', 'id_formulaire=' . intval($reponse['id_formulaire']));

		$traitements = (function_exists('formidable_deserialize') ? formidable_deserialize($formulaire['traitements']) : unserialize($formulaire['traitements']));
		if ($traitements and isset($traitements['paiement'])){
			$champs_payeur = [
				'auteur' => 'email',
				'nom' => 'nom',
				'prenom' => 'prenom',
				'adresse' => 'adresse',
				'code_postal' => 'code_postal',
				'ville' => 'ville',
				'pays' => 'pays',
			];
			foreach ($champs_payeur as $champ_payeur => $info){
				if (isset($traitements['paiement']['champ_' . $champ_payeur])
					and $nom_champ = $traitements['paiement']['champ_' . $champ_payeur]){
					if ($valeur = calculer_voir_reponse($id_formulaires_reponse, $reponse['id_formulaire'], $nom_champ, '', 'brut')){
						$flux['data'][$info] = $valeur;
					}
				}
			}
		}
	}

	return $flux;
}


/**
 * Ajouter le message de retour post-paiement
 * @param array $flux
 * @return array
 */
function formidablepaiement_bank_traiter_reglement($flux){
	// si c'est une transaction associee a un form
	if ($id_transaction = $flux['args']['id_transaction']
		and $id_formulaire = formidablepaiement_is_parrain_formidable($flux['args']['avant']['parrain'])
		and $id_formulaires_reponse = $flux['args']['avant']['tracking_id']){

		include_spip('formidable_fonctions');

		$reponse = sql_fetsel('*', 'spip_formulaires_reponses', 'id_formulaires_reponse=' . intval($id_formulaires_reponse));
		$formulaire = sql_fetsel('*', 'spip_formulaires', 'id_formulaire=' . intval($reponse['id_formulaire']));

		$traitements = (function_exists('formidable_deserialize') ? formidable_deserialize($formulaire['traitements']) : unserialize($formulaire['traitements']));
		if ($traitements
			and !empty($traitements['paiement']['message'])
			and $message = trim($traitements['paiement']['message'])){
			include_spip("inc/texte");
			$flux['data'] .= propre($message);
		}
	}

	return $flux;
}

function formidablepaiement_affiche_enfants($flux){

	if (isset($flux['args']['objet'])
		and $flux['args']['objet']=='formulaires_reponse'
		and $id_formulaires_reponse = $flux['args']['id_objet']){

		$reponse = sql_fetsel('*', 'spip_formulaires_reponses', 'id_formulaires_reponse=' . intval($id_formulaires_reponse));

		$parrain = formidablepaiement_parrain($reponse['id_formulaire']);
		$where = "parrain=" . sql_quote($parrain) . " AND tracking_id=" . intval($id_formulaires_reponse);
		$flux['data'] .= "<div class='nettoyeur'></div>" . recuperer_fond("prive/objets/liste/transactions", array('where' => $where));
	}
	return $flux;
}

function formidablepaiement_affiche_milieu($flux){
	if ($flux['args']['exec']=='formulaires_reponses'
		and $id_formulaire = $flux['args']['id_formulaire']){

		$parrain = formidablepaiement_parrain($id_formulaire);
		$where = "parrain=" . sql_quote($parrain);
		$flux['data'] .= "<div class='nettoyeur'></div>" . recuperer_fond("prive/objets/liste/transactions", array('where' => $where));
	}
	return $flux;
}
